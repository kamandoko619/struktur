-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Sep 2016 pada 14.52
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grails_struktur`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `grup`
--

CREATE TABLE `grup` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `level_id` bigint(20) DEFAULT NULL,
  `ngrup` varchar(255) NOT NULL,
  `delete_flag` varchar(255) NOT NULL,
  `batas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `grup`
--

INSERT INTO `grup` (`id`, `version`, `level_id`, `ngrup`, `delete_flag`, `batas`) VALUES
(1, 0, NULL, 'Ceo', 'N', 1),
(2, 2, 1, 'instictz', 'N', 2),
(3, 1, 1, 'Valor', 'N', 2),
(4, 4, 1, 'Rocket', 'N', 3),
(5, 0, 1, 'instict', 'N', 4),
(6, 3, 1, 'teravin', 'N', 4),
(7, 3, 2, 'Valoro', 'N', 4),
(8, 3, 4, 'Mystic', 'N', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` bigint(20) NOT NULL,
  `version` bigint(20) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `delete_flag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `version`, `jabatan`, `nama`, `delete_flag`) VALUES
(1, 36, 8, 'mukidi', 'N'),
(2, 15, 2, 'kamandoko', 'N'),
(3, 20, 2, 'Tumini', 'N'),
(4, 3, 3, 'Sumanto', 'N'),
(5, 9, 7, 'Paijo', 'N'),
(6, 8, 3, 'ryu', 'N'),
(7, 6, 8, 'Unyil', 'N'),
(8, 1, 4, 'sasuke', 'N'),
(9, 1, 4, 'naruto', 'N'),
(10, 1, 1, 'sakura', 'N'),
(11, 4, 0, 'kakashi', 'N'),
(12, 1, 5, 'obito', 'N'),
(13, 1, 5, 'neji', 'N'),
(14, 1, 5, 'hinata', 'N'),
(15, 0, 0, 'kurenai', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK308CC6408FA038` (`level_id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grup`
--
ALTER TABLE `grup`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `grup`
--
ALTER TABLE `grup`
  ADD CONSTRAINT `FK308CC6408FA038` FOREIGN KEY (`level_id`) REFERENCES `grup` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
