package han.domen

class GrupController {

    def index() {
        redirect(action: "list")
    }
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def grupInstanceList= Grup.list()
        def grupInstance = Grup.createCriteria().list(params) {
            if ( params.nmgrup ) {
                ilike("nmgrup", "%${params.nmgrup}%")
                and {
                    eq("deleteFlag","N")
                }
            }
            eq("deleteFlag","N")
        }
        def jumlahanggota= []
        int jmlgrup = Grup.count()
        int i
        for (i=1;i<=jmlgrup;i++)
        {
            int jmlanggotagrup =  Pegawai.countByJabatan(i)
            jumlahanggota.add(jmlanggotagrup)
        }
        [grupInstance: grupInstance, grupInstanceCount: grupInstance.size(),jumlahanggota:jumlahanggota]

    }
    def add(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [grupInstance: new Grup(params),grupInstanceList: Grup.list(params), grupInstanceTotal: Grup.count()]

    }
    def simpan() {

        def grupInstance
        def ambilmax = params.batas
        def intAmbilmax = ambilmax.toInteger()

        if (params.level == "null"){
            grupInstance = new Grup(ngrup: params.ngrup)
        }
        else {
            def parentObject = Grup.get(params.level)
            grupInstance = new Grup(ngrup: params.ngrup, level: parentObject,batas: intAmbilmax)
        }

        println params
        if (!grupInstance.save()) {
            render(view: "add", model: [grupInstance: grupInstance])
            return
        }
        redirect(action: "show", id: grupInstance.id)

    }
    def show(Long id) {
        def grupInstance = Grup.get(id)
        def grupInstance2 = Grup.get(grupInstance.level.id)

        if (!grupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grup.label', default: 'Grup'), id])
            redirect(action: "show", id: grupInstance.id)
            return
        }

        [grupInstance:grupInstance,grupInstance2:grupInstance2]
    }
    def tree(Integer max, Long id) {
        params.max = Math.min(max ?: 10, 100)
        def grupInstanceList= Grup.list()
        def pegawaiInstanceList= Pegawai.list()
        println params

////
        if(id > 0 ){
            def pegawaiInstance = Pegawai.get(params.id)
            def strnumber = params.jabatan
            def intValue = strnumber.toInteger()
            pegawaiInstance.jabatan = intValue
        pegawaiInstance.save(flush: true)
        redirect(action:  "tree")
        }
        def jumlahanggota= []
        int jmlgrup = Grup.count()
        int i
        for (i=1;i<=jmlgrup;i++)
        {
            int jmlanggotagrup =  Pegawai.countByJabatan(i)
            jumlahanggota.add(jmlanggotagrup)
        }

        [grupInstanceList: grupInstanceList, grupInstanceTotal: Grup.count(),pegawaiInstanceList:pegawaiInstanceList,pegawaiInstanceTotal: Pegawai.count(),jumlahanggota:jumlahanggota]
    }
    def edit(Long id) {
        def grupInstance = Grup.get(id)
        if (id>1){
            def grupInstance2 = Grup.get(grupInstance.level.id)
            [grupInstance:grupInstance,grupInstance2:grupInstance2,grupInstanceList: Grup.list(params)]
        }
        else {
            def grupInstance2 = Grup.get(id)
            [grupInstance:grupInstance,grupInstance2:grupInstance2,grupInstanceList: Grup.list(params)]
        }

    }

    def ubah(Long id, Long version) {

        def grupInstance = Grup.get(params.id)
        def lepel = Grup.get(params.level)
        def batasstring = params.batas
        def batas = batasstring.toInteger()
        grupInstance.level = lepel
        grupInstance.batas = batas
        grupInstance.ngrup = params.ngrup

        println params
        grupInstance.save(flush: true)
        redirect(action:  "index")
//        def grupInstance = Grup.get(id)
//        if (!grupInstance) {
//            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grup.label', default: 'Grup'), id])
//            redirect(action: "list")
//            return
//        }
//
//        if (version != null) {
//            if (grupInstance.version > version) {
//                grupInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
//                        [message(code: 'grup.label', default: 'Grup')] as Object[],
//                        "Another user has updated this Book while you were editing")
//                render(view: "edit", model: [grupInstance: grupInstance])
//                return
//            }
//        }
//
//        grupInstance.properties = params
//
//        if (!grupInstance.save(flush: true)) {
//            render(view: "edit", model: [grupInstance: grupInstance])
//            return
//        }
//
//        flash.message = message(code: 'default.updated.message', args: [message(code: 'grup.label', default: 'Book'), grupInstance.id])
//        redirect(action: "show", id: grupInstance.id)
    }

    def delete(){
        def eventInstance = Grup.get(params.id)
        eventInstance.deleteFlag = "Y"
        eventInstance.save()
        redirect(action:  "index")
    }
    def tampilidcabang(long id,long idjb){
        println params

    }
}
