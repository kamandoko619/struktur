package han.domen

class PegawaiController {

    def index() {
        redirect(action: "list")
    }
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def pegawaiInstanceList= Pegawai.list()
        def pegawaiInstance = Pegawai.createCriteria().list(params) {
            if ( params.nama ) {
                ilike("nama", "%${params.nama}%")
                and {
                    eq("deleteFlag","N")
                }
            }
            eq("deleteFlag","N")
        }

        [pegawaiInstance: pegawaiInstance, pegawaiInstanceCount: pegawaiInstance.size()]

    }
    def add(Integer max) {

    }
    def simpan() {

        def grupInstance
        def ambilnama = params.nama
        def ambiljabatan = params.jabatan
        def intAmbiljabatan = ambiljabatan.toInteger()

            grupInstance = new Pegawai(nama: ambilnama, jabatan: intAmbiljabatan)

        println params
        if (!grupInstance.save()) {
            render(view: "add", model: [grupInstance: grupInstance])
            return
        }
        redirect(action: "show", id: grupInstance.id)

    }
    def show(Long id) {
        def pegawaiInstance = Pegawai.get(id)

        if (!pegawaiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pegawai.label', default: 'Pegawai'), id])
            redirect(action: "show", id: pegawaiInstance.id)
            return
        }

        [pegawaiInstance:pegawaiInstance]
    }
}
