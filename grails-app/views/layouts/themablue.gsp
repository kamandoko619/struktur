<%--
  Created by IntelliJ IDEA.
  User: handoko
  Date: 12/08/2016
  Time: 10.52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projek</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->


    <style>
    @media screen and (max-width:2024px) {

    }
    @media screen and (max-width:1024px) {

    }
    @media screen and (max-width:991px) {

    }
    @media screen and (min-width:940px) {

    }
    @media screen and (min-width:820px) {
        .land{
            display: none;
        }
        .pot{
            display: inherit;
            padding-top: 10%;
        }
    }
    @media screen and (max-width:820px) {
        .land{
            display: inherit;
        }
        .pot{
            display: none;
        }

    }
    @media screen and (max-width:520px){
        .land{
            display: none;
        }
        .pot{
            display: inherit;
            padding-top: 30%;
        }
    }
    @media screen and (max-width:100px){

    }


    html, body{
        height: 100%;
    }


    div, body{
        margin: 0;
        padding: 0;
        font-family: exo, sans-serif;

    }
    .wrapper {
        height: 100%;
        width: 100%;
    }

    .logokanan{
        border-bottom: 110px solid #009fe3;
        border-left: 70px solid transparent;
    }
    .kotak{
        width: 100%;
        height: 100%;
        background: #dbdfe0;
        position: relative;
        -moz-border-radius:    10px;
        -webkit-border-radius: 10px;
        border-radius:         10px;
        margin-top: 10px;
    }

    </style>


</head>
<body>
<div class="container">
    <div class="logokiri" style="width: 50%;height: 130px;margin-top: 50px;">
        <table  style="height: 100%;width: 100%">
            <tr>
                <td style="text-align: center;font-size: 50px">
                    LOGO
                </td>
            </tr>
        </table>
    </div>
    <div class="logokanan" style="width: 55%;height: 110px;margin-top: -130px;margin-left: 45%">
        <table  style="height: 110px;width: 100%;">
            <tr>
                <td style="text-align: center;font-size: 50px">
                    WELCOM
                </td>
            </tr>
        </table>
    </div>


    <div class="col-md-3" style="padding-top: 40px;">
        <div class="kotak" >



            <div style="text-align: left; padding-top: 10px; padding-left: 20px;">
                <div class="dropdown">
                    <a class=" dropdown-toggle" type="button" data-toggle="dropdown" style="color: #00adef">Grop
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><g:link  controller="grup" action="add">Add Grup </g:link></li>
                        <li><g:link  controller="grup" action="list">List Grup </g:link></li>
                        <li><g:link  controller="grup" action="tree">lihat tree</g:link></li>



                    </ul>
                </div>

            </div>
            <hr class="kotak"  align="center" style="height: 1px;background-color: white;width: 90%;" >
            <div style="text-align: left; padding-top: 10px; padding-left: 20px;">
                <div class="dropdown">
                    <a class=" dropdown-toggle" type="button" data-toggle="dropdown" style="color: #00adef">Pegawai
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><g:link  controller="pegawai" action="add">Add Pegawai </g:link></li>
                        <li><g:link  controller="pegawai" action="list">List Pegawai </g:link></li>



                    </ul>
                </div>

            </div>
            <hr class="kotak"  align="center" style="height: 1px;background-color: white;width: 90%;" >



        </div>
    </div>





    <div class="col-md-9 " style="margin-top: 10px">
        <table style="width: 100%; ">
            <tr>
                <td style="text-align: right ; color:  #009fe3;" >
                    IN  <span style="color:black">|</span>EN

                </td>
            </tr>
            <tr>
                <td>

                    <g:layoutBody/>

                </td>
            </tr>

        </table>
    </div>


</div>
<div class="container">
    <div class="col-md-6 " style="margin-top: 10px;font-size: 13px">
        <span style="color: #284e9f">Licence To </span><span style="color: #009fe3">Pt. bla bla bla </span>
    </div>
    <div class="col-md-3 " style="margin-top: 10px;font-size: 13px">
        <table border="" style="width: 100%;height: 70px;">
            <tr>
                <td style="text-align: center;font-size: 20px;font-weight: bold">
                    Icon
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-3 " style="margin-top: 10px;font-size: 13px">
        <table border="" style="width: 100%;height: 70px;">
            <tr>
                <td style="text-align: center;font-size: 20px;font-weight: bold">
                    Icon
                </td>
            </tr>
        </table>
    </div>

</div>









<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>





</body>
</html>