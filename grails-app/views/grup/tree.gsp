<%--
  Created by IntelliJ IDEA.
  User: handoko
  Date: 12/08/2016
  Time: 10.52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title> Basic example </title>
    <link rel="stylesheet" href="${resource(dir: 'lib', file: 'Treant.css')}">
    <link rel="stylesheet" href="${resource(dir: 'basic', file: 'basic-example.css')}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->


    <style>
    @media screen and (max-width:2024px) {

    }
    @media screen and (max-width:1024px) {

    }
    @media screen and (max-width:991px) {

    }
    @media screen and (min-width:940px) {

    }
    @media screen and (min-width:820px) {
        .land{
            display: none;
        }
        .pot{
            display: inherit;
            padding-top: 10%;
        }
    }
    @media screen and (max-width:820px) {
        .land{
            display: inherit;
        }
        .pot{
            display: none;
        }

    }
    @media screen and (max-width:520px){
        .land{
            display: none;
        }
        .pot{
            display: inherit;
            padding-top: 30%;
        }
    }
    @media screen and (max-width:100px){

    }


    html, body{
        height: 100%;
    }


    div, body{
        margin: 0;
        padding: 0;
        font-family: exo, sans-serif;

    }
    .wrapper {
        height: 100%;
        width: 100%;
    }

    .logokanan{
        border-bottom: 110px solid #009fe3;
        border-left: 70px solid transparent;
    }
    .kotak{
        width: 100%;
        height: 100%;
        background: #dbdfe0;
        position: relative;
        -moz-border-radius:    10px;
        -webkit-border-radius: 10px;
        border-radius:         10px;
        margin-top: 10px;
    }

    </style>


    <style type="text/css">
    /*css hide show*/

    #juliocaesarFX{ position:fixed; top:30px; z-index:+1000; }
    * html #juliocaesarFX{position:relative;}
    .juliocaesarFXtab {
        height: 108px;
        width: 37px;
        float: right;
        cursor: pointer;
    }
    .juliocaesarFXcontent{
        width: 200px;;
        float:right;
        border:2px solid #000000;
        -moz-border-radius-topleft:5px;
        -moz-border-radius-topright:5px;
        -moz-border-radius-bottomleft:5px;
        -moz-border-radius-bottomright:5px;
        background:whitesmoke;
        padding:10px; }
    </style>


<script type="text/javascript">
    var addUser = "${createLink(url: [controller: 'grup', action: 'tampilidcabang'])}?id=10";
</script>
</head>
<body>
<div id="juliocaesarFX"> <div class="juliocaesarFXtab" onclick="showHidejuliocaesarFX()">
    <button style="margin-left: 10px;" >
    <table>
        <tr>
            <td>
                <img src="${resource(dir: 'images/icon/',file: 'menu.png' )}" style="height: 50px;width: 50px;">

            </td>
        </tr>
        <tr>
            <td>
                Menu

            </td>
        </tr>
    </table>
</button>
</div>
    <div class="juliocaesarFXcontent">
        <g:link  controller="grup" action="add"><span class="glyphicon glyphicon-home" aria-hidden="true" style="padding-right: 10px;"> </span>Home </g:link>
        <div class="dropdown" style="padding-top: 5px;">
            <a class=" dropdown-toggle" type="button" data-toggle="dropdown" style="color: #00adef"><span class="glyphicon glyphicon-briefcase" aria-hidden="true" style="padding-right: 10px;"> </span>Grop
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><g:link  controller="grup" action="add">Add Grup </g:link></li>
                <li><g:link  controller="grup" action="list">List Grup </g:link></li>
                <li><g:link  controller="grup" action="tree">lihat tree</g:link></li>



            </ul>
        </div>
        <div class="dropdown" style="padding-top: 5px;">
            <a class=" dropdown-toggle" type="button" data-toggle="dropdown" style="color: #00adef"><span class="glyphicon glyphicon-user" aria-hidden="true" style="padding-right: 10px;"></span>
                Pegawai
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><g:link  controller="pegawai" action="add">Add Pegawai </g:link></li>
                <li><g:link  controller="pegawai" action="list">List Pegawai </g:link></li>



            </ul>
        </div>
        <div style="text-align:right">
        <a href="javascript:showHidejuliocaesarFX()"> <span class="glyphicon glyphicon-remove" aria-hidden="true"> </span> close </a> </div> </div> </div>
<center>

       <table hidden>
           <tr>
               <td>
                   <table class="table table-bordered " border="1" >
                       <thead>
                       <tr>
                           <td>
                               ID grup
                           </td>
                           <td>Nama grup
                           </td>
                           <td>
                               Parent Grup
                           </td>
                           <td>
                               Maxkapasitas
                           </td>


                       </tr>
                       </thead>
                       <tbody>
                       <g:each var="team" in="${grupInstanceList}">
                           <tr>
                               <td>
                                   <g:textField name="bookTitle" id="idjabatan${team.id}" value="${team.id}" disabled=""/>


                               </td>
                               <td>
                                   <g:textField name="bookTitle" id="nmjabatan${team.id}" value="${team.ngrup}" disabled=""/>

                               </td>
                               <td>
                                   <g:if test="${team.level == null}">
                                       <g:textField name="bookTitle"  value="Null" disabled=""/>
                                       <g:hiddenField name="parentjbt${team.id}" id="parentjbt${team.id}" value="0" />

                                   </g:if>
                                   <g:else>
                                       <g:textField name="bookTitle"  value="${team.level.ngrup}" disabled=""/>
                                       <g:hiddenField name="parentjbt${team.id}" id="parentjbt${team.id}" value="${team.level.id}" />



                                   </g:else>
                               </td>
                               <td>
                                   <g:textField name="bookTitle" id="maxjabatan${team.id}" value="${team.batas}" disabled=""/>


                               </td>
                           </tr>
                       </g:each>
                       </tbody>
                   </table>
               </td>
               <td>
                   <g:each var="jum"  in="${jumlahanggota}" status="counter">
                       <g:textField name="bookTitle" id="jumlahpgwgrup${counter+1}" value="${jum}" disabled=""/>

                   </g:each>

               </td>
               <td style="padding-left: 10px;">
                   <table class="table table-bordered " border="1" >
                       <thead>
                       <tr>
                           <td>
                               ID
                           </td>
                           <td>Nama
                           </td>
                           <td>
                               Jabatan
                           </td>


                       </tr>
                       </thead>
                       <tbody>
                       <g:each var="pgw" in="${pegawaiInstanceList}">
                           <tr>
                               <td>
                                   <g:textField name="bookTitle" id="idpegawai${pgw.id}" value="${pgw.id}" disabled=""/>


                               </td>
                               <td>
                                   <g:textField name="bookTitle" id="nmpegawai${pgw.id}" value="${pgw.nama}" disabled=""/>

                               </td>
                               <td>
                                   <g:if test="${pgw.jabatan == null}">
                                       <g:textField name="bookTitle"  value="Null" disabled=""/>

                                   </g:if>
                                   <g:else>
                                       <g:textField name="bookTitle" id="pgwjabatan${pgw.id}" value="${pgw.jabatan}" disabled=""/>



                                   </g:else>
                               </td>

                           </tr>
                       </g:each>
                       <g:textField name="bookTitle" id="jumlahpgw" value="${pegawaiInstanceTotal}" hidden=""/>

                       </tbody>
                   </table>
               </td>
           </tr>
       </table>


    <g:textField name="bookTitle" id="jumlah" value="${grupInstanceTotal}" hidden=""/>
       <table border="" style="">
           <tr>
               <td style="width: 1100px;height: 650px">
                   <div >
                   <div class="chart" id="basic-example" style=""></div>
   </div>
               </td>
               <td style="vertical-align: top;width: 200px">
                   <table style="">
                       <tr>
                           <td style="text-align: center;font-weight: bold;background-color: #81c7ff;height: 50px;">
                               <table style="margin-left: 10px;">
                                   <tr>
                                       <td>
                                           <img src="${resource(dir: 'images/icon', file: 'allpegawai.png')}" style="height: 40px;margin-left: 10px;">

                                       </td>
                                       <td>
                                           <p style="margin-top: 10px;margin-left: 10px"> List Pegawai</p>

                                       </td>
                                   </tr>
                               </table>



                           </td>
                       </tr>
                       <tr>
                           <td  style="padding-top: 10px;text-align: center">
                               <form id="formbasic" action="tree" method="post">
                                   <div  ondrop="drop(event)" ondragover="allowDrop(event)" style="background-color: whitesmoke;height: 50px;width: 200px;margin-top: -10px;">
                                       <table style="margin-left: 10px;">
                                           <tr>
                                               <td style="">
                                                   <img src="${resource(dir: 'images/icon', file: 'Droppegawai.png')}" style="height: 40px;margin-left: 10px;margin-top: 5px">

                                               </td>
                                               <td>
                                                   <p style="margin-top: 12px;margin-left: 10px;font-weight: bold"> Drop Here </p>

                                               </td>
                                           </tr>
                                       </table>



                                       <input type="text" name="jabatan" value="0" hidden>
                                   </div>


                               </form>
                               <div class="scroll2" style="background-color: white;">

                                   <g:each var="pgw" in="${pegawaiInstanceList}">
                                       <div  id="${pgw.id}" draggable="true" ondragstart="drag(event)">
                                           <div class="talkbubble">
                                               <center>
                                                   <g:if test="${pgw.jabatan==0 }">
                                                       <div style="background-color: greenyellow;height: 20px;width: 170px;">

                                                           ${pgw.nama}
                                                           <input type="text" name="id" value="${pgw.id}" hidden >
                                                           <input type="text" name="nama" value="${pgw.nama}" hidden >







                                                       </div>
                                                   </g:if>
                                               </center>
                                           </div>
                                       </div>
                                   </g:each>


                               </div>



                           </td>
                       </tr>
                   </table>
               </td>
           </tr>
       </table>




</center>


<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="${resource(dir: 'lib/vendor',file: 'raphael.js' )}"></script>
<script type="text/javascript" src="${resource(dir: 'lib',file: 'Treant.js' )}"></script>

<script type="text/javascript" src="${resource(dir: 'basic',file: 'basic-example.js' )}"></script>
<script type="text/javascript">
    //hide show menu
    function showHidejuliocaesarFX(){
        var juliocaesarFX = document.getElementById("juliocaesarFX");
        var w = juliocaesarFX.offsetWidth;
        juliocaesarFX.opened ? movejuliocaesarFX(0, 40-w) : movejuliocaesarFX(40-w, 0); juliocaesarFX.opened = !juliocaesarFX.opened; }
    function movejuliocaesarFX(x0, xf){
        var juliocaesarFX = document.getElementById("juliocaesarFX");
        var dx = Math.abs(x0-xf) > 10 ? 5 : 1;
        var dir = xf>x0 ? 1 : -1;
        var x = x0 + dx * dir;
        juliocaesarFX.style.left = x.toString() + "px";
        if(x0!=xf){setTimeout("movejuliocaesarFX("+x+", "+xf+")", 10);} }
    var juliocaesarFX = document.getElementById("juliocaesarFX"); juliocaesarFX.style.left = (40-juliocaesarFX.offsetWidth).toString() + "px";

</script>
<script>

    var jumlah22 = document.getElementById("jumlah").value;

    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    }



    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
        alert('simpan perubahan 8');
        document.getElementById('formbasic').submit();

    }


    var funcs = [];

    for (var i = 1; i <= jumlah22; i++) {
        funcs[i] = (function(index) {
            var jabatan2 = document.getElementById("nmjabatan"+i).value;
            return function(ev) {
                ev.preventDefault();
                var data = ev.dataTransfer.getData("text");
                ev.target.appendChild(document.getElementById(data));
                alert('simpan perubahan jabatan '+jabatan2);
                document.getElementById('form'+index).submit();
            };
        }(i));
    }





</script>
<script>
    new Treant( chart_config );

    var jumlahpgwall = document.getElementById("jumlahpgw").value;
    var jumlah2 = document.getElementById("jumlah").value;
    for (i=1;i<=jumlah2;i++) {
        var jabatan = document.getElementById("nmjabatan"+i).value;
        var idjabatan = document.getElementById("idjabatan"+i).value;
        var nilaimaxjabatan = document.getElementById("maxjabatan"+i).value;
        var nilaidipakaijabatan = document.getElementById("jumlahpgwgrup"+i).value;



        if(i==1) {
            if(nilaidipakaijabatan==nilaimaxjabatan){
                var div = document.createElement('div');
                div.className = 'rowceo';
                div.innerHTML = '<div style="width: 150px;height: 100px;margin-top: -10px;"><table style="width: 100%"><tr><td style="text-align: center"><div id="rowpgwtree'+i+'" class="scroll" style="height: 113px;"></div></td></tr><tr><td style="text-align: center"><div style="background-color: whitesmoke"> <center>'+nilaidipakaijabatan+'/'+nilaimaxjabatan+'</center></div><div  style="background-color: #ffdc48;height: 20px;width: 165px;margin-top: -3px;margin-left: -0.5px;font-weight: bold;"><a href="#"> <p style="padding-top: 2px;">Full </p></a></div></td></tr></table><div>';
                document.getElementById('ceo').appendChild(div);
                for(var c=1;c<=jumlahpgwall;c++)
                {
                    var nmpgwjabatan= document.getElementById("pgwjabatan"+c).value;

                    if(nmpgwjabatan==idjabatan){
                        var nmpgw= document.getElementById("nmpegawai"+c).value;
                        var idpgw= document.getElementById("idpegawai"+c).value;

                        var divpgw = document.createElement('div');
                        divpgw.className = 'rowtree'+c;
                        divpgw.innerHTML = '<div  id="'+idpgw+'" draggable="true" ondragstart="drag(event)"> <div class="talkbubble"> <center> <div style="background-color: greenyellow;height: 20px;width: 140px;margin-left: -6px;">'+nmpgw+' <input type="text" name="id" value="'+idpgw+'" hidden ><input type="text" name="nama" value="'+nmpgw+'" hidden > </div> </center> </div> </div>';
                        document.getElementById('rowpgwtree'+i).appendChild(divpgw);
                    }
                }
            }
            else{
                var div = document.createElement('div');
                div.className = 'rowceo';
                div.innerHTML = '<div style="width: 150px;height: 100px;margin-top: -10px;background-color: white"><table style="width: 100%"><tr><td style="text-align: center"><div id="rowpgwtree'+i+'" class="scroll" style="height: 113px;"></div></td></tr><tr><td style="text-align: center"><form id="form'+i+'" action="tree" method="post"><div style="background-color: whitesmoke"> <center>'+nilaidipakaijabatan+'/'+nilaimaxjabatan+'</center></div><div  style="background-color: #ffdc48;height: 20px;width: 165px;margin-top: -3px;margin-left: -0.5px;font-weight: bold;" ondrop="funcs[1](event)" ondragover="allowDrop(event)"><a href="'+addUser+'&idjb='+i+'"> <p style="padding-top: 2px;"> <span class="glyphicon glyphicon-hand-right" aria-hidden="true" style="padding-right: 10px;"> </span>Drop Here/ Klik </p></a></div><input type="text" name="jabatan" value="'+idjabatan+'" hidden></form></td></tr></table><div>';
                document.getElementById('ceo').appendChild(div);
                for(var c=1;c<=jumlahpgwall;c++)
                {
                    var nmpgwjabatan= document.getElementById("pgwjabatan"+c).value;

                    if(nmpgwjabatan==idjabatan){
                        var nmpgw= document.getElementById("nmpegawai"+c).value;
                        var idpgw= document.getElementById("idpegawai"+c).value;

                        var divpgw = document.createElement('div');
                        divpgw.className = 'rowtree'+c;
                        divpgw.innerHTML = '<div  id="'+idpgw+'" draggable="true" ondragstart="drag(event)"> <div class="talkbubble"> <center> <div style="background-color: greenyellow;height: 20px;width: 140px;margin-left: -6px;">'+nmpgw+' <input type="text" name="id" value="'+idpgw+'" hidden ><input type="text" name="nama" value="'+nmpgw+'" hidden > </div> </center> </div> </div>';
                        document.getElementById('rowpgwtree'+i).appendChild(divpgw);
                    }
                }
            }
        }
        else{
            if(nilaidipakaijabatan==nilaimaxjabatan) {
                var div2 = document.createElement('div');
                div2.className = 'jabatan'+i;
                div2.innerHTML = '<div style="width: 150px;height: 100px;margin-top: -10px;"><table style="width: 100%"><tr><td style="text-align: center"><div id="rowpgwtree'+i+'" class="scroll" style="height: 113px;"></div></td></tr><tr><td style="text-align: center"><div style="background-color: whitesmoke"> <center>'+nilaidipakaijabatan+'/'+nilaimaxjabatan+'</center></div><div  style="background-color: #ffdc48;height: 20px;width: 165px;margin-top: -3px;margin-left: -0.5px;font-weight: bold;" ><a href="'+addUser+'&idjb='+i+'"> <p style="padding-top: 2px;">Full </p></a></div></td></tr></table><div>';
                document.getElementById('jabatan'+i).appendChild(div2);
                for(var c=1;c<=jumlahpgwall;c++)
                {
                    var nmpgwjabatan= document.getElementById("pgwjabatan"+c).value;

                    if(nmpgwjabatan==idjabatan){
                        var nmpgw= document.getElementById("nmpegawai"+c).value;
                        var idpgw= document.getElementById("idpegawai"+c).value;

                        var divpgw = document.createElement('div');
                        divpgw.className = 'rowtree'+c;
                        divpgw.innerHTML = '<div  id="'+idpgw+'" draggable="true" ondragstart="drag(event)"> <div class="talkbubble"> <center> <div style="background-color: greenyellow;height: 20px;width: 140px;margin-left: -6px;">'+nmpgw+' <input type="text" name="id" value="'+idpgw+'" hidden ><input type="text" name="nama" value="'+nmpgw+'" hidden > </div> </center> </div> </div>';
                        document.getElementById('rowpgwtree'+i).appendChild(divpgw);
                    }
                }
            }
            else{
                var div2 = document.createElement('div');
                div2.className = 'jabatan'+i;
                div2.innerHTML = '<div style="width: 150px;height: 100px;margin-top: -10px;background-color: white"><table style="width: 100%"><tr><td style="text-align: center"><div id="rowpgwtree'+i+'" class="scroll" style="height: 113px;"></div></td></tr><tr><td style="text-align: center"><form id="form'+i+'" action="tree" method="post"><div style="background-color: whitesmoke"> <center>'+nilaidipakaijabatan+'/'+nilaimaxjabatan+'</center></div><div  style="background-color: #ffdc48;height: 20px;width: 165px;margin-top: -3px;margin-left: -0.5px;font-weight: bold;" ondrop="funcs['+i+'](event)" ondragover="allowDrop(event)"><a href="'+addUser+'&idjb='+i+'"> <p style="padding-top: 2px;"> <span class="glyphicon glyphicon-hand-right" aria-hidden="true" style="padding-right: 10px;"> </span>Drop Here/ Klik </p></a></div><input type="text" name="jabatan" value="'+idjabatan+'" hidden></form></td></tr></table><div>';
                document.getElementById('jabatan'+i).appendChild(div2);
                for(var c=1;c<=jumlahpgwall;c++)
                {
                    var nmpgwjabatan= document.getElementById("pgwjabatan"+c).value;

                    if(nmpgwjabatan==idjabatan){
                        var nmpgw= document.getElementById("nmpegawai"+c).value;
                        var idpgw= document.getElementById("idpegawai"+c).value;

                        var divpgw = document.createElement('div');
                        divpgw.className = 'rowtree'+c;
                        divpgw.innerHTML = '<div  id="'+idpgw+'" draggable="true" ondragstart="drag(event)"> <div class="talkbubble"> <center> <div style="background-color: greenyellow;height: 20px;width: 140px;margin-left: -6px;">'+nmpgw+' <input type="text" name="id" value="'+idpgw+'" hidden ><input type="text" name="nama" value="'+nmpgw+'" hidden > </div> </center> </div> </div>';
                        document.getElementById('rowpgwtree'+i).appendChild(divpgw);
                    }
                }

            }

        }
    }

</script>
</body>
</html>