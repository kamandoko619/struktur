<%--
  Created by IntelliJ IDEA.
  User: handoko
  Date: 12/08/2016
  Time: 10.52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="layout" content="themablue">

    <title>Projek</title>

</head>
<body>

                                            <table class="" >
                                                <thead>
                                                <tr>
                                                    <td style="width: 100px;text-align: center;font-weight: bold;font-size: 16px;">
                                                       Nama Grup

                                                    </td>
                                                    <td style="width: 100px;text-align: center;font-weight: bold;font-size: 16px;">
                                                    Parent Grup
                                                </td>
                                                    <td style="width: 100px;text-align: center;font-weight: bold;font-size: 16px;">
                                                        Terpakai
                                                    </td>
                                                    <td style="width: 100px;text-align: center;font-weight: bold;font-size: 16px;">
                                                        Max
                                                    </td>
                                                    <td style="width: 150px;text-align: center;font-weight: bold;font-size: 16px;">
                                                        Action
                                                    </td>


                                                </tr>
                                                </thead>
                                                <tbody>
                                                <g:each var="team" in="${grupInstance}" status="counter">
                                                    <tr>
                                                        <td style="width: 150px;text-align: center;height: 40px;">${team.ngrup}</td>
                                                        <td style="width: 150px;text-align: center">
                                                            <g:if test="${team.level == null}">
                                                                -
                                                            </g:if>
                                                            <g:else>
                                                                ${team.level.ngrup}
                                                            </g:else>
                                                        </td>
                                                        <td style="width: 150px;text-align: center;height: 40px;">${jumlahanggota[counter]}</td>
                                                        <td style="width: 150px;text-align: center;height: 40px;">${team.batas}</td>

                                                        <td style="text-align: center;">
                                                    <g:form>
                                                        <fieldset class="buttons" >
                                                        <g:hiddenField name="id" value="${team.id}" />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <g:actionSubmit class="delete btn btn-info" action="edit" value="Edit"  style="width: 60px;height: 25px;font-size: 12px;" />

                                                                </td>
                                                                <td style="padding-left: 10px;">
                                                                    <g:actionSubmit class="delete btn btn-danger" action="delete" value="Hapus" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" style="width: 60px;height: 25px;font-size: 12px;" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        </fieldset>
                                                    </g:form>
                                                        </td>

                                                    </tr>
                                                </g:each>
                                                </tbody>
                                            </table>

                                            <table class="table table-hovered">
                                                <tr>
                                                    <td style="text-align: right;">
                                                        <g:link class="create btn btn-default" action="add"> Tambah data </g:link>
                                                    </td>
                                                </tr>
                                            </table>



</body>
</html>