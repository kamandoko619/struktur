<%--
  Created by IntelliJ IDEA.
  User: handoko
  Date: 12/08/2016
  Time: 10.52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="layout" content="themablue">



</head>
<body>

                    <div class="kotak" style="padding-bottom: 30px;padding-top: 5px">
                        <div id="create-author" class="content scaffold-create" role="main">
                            <center>
                                <div id="show-author" class="content scaffold-show" role="main">
                                    <center>

                                        <table style="width: 300px;margin-top: 20px;" >
<g:form action="ubah" id="${grupInstance.id}" method="post" class="form-horizontal">
                                            <g:if test="${grupInstance?.ngrup}">
                                                <tr>
                                                    <td colspan="3" style="text-align: center;font-weight: bold;font-size: 20px;">
                                                        Data <g:fieldValue bean="${grupInstance}" field="ngrup"/>
                                                    </td>
                                                </tr>
                                            </g:if>
                                            <g:if test="${grupInstance?.ngrup}">
                                                <tr>
                                                    <td style="font-weight: bold;width: 100px;height: 30px;">
                                                        <g:message code="grup.ngrup.label" default="Ngrup" />
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td>
                                                        <input type="text" name="ngrup" id="ngrup" value="${grupInstance.ngrup}">
                                                    </td>
                                                </tr>
                                                <g:if test="${grupInstance.ngrup=="Ceo"}">

                                                </g:if>
                                                <g:else>
                                                    <g:if test="${grupInstance2?.ngrup}">
                                                        <tr>
                                                            <td style="font-weight: bold;width: 100px;height: 30px;">
                                                                Parent
                                                            </td>
                                                            <td style="font-weight: bold;">
                                                                :
                                                            </td>
                                                            <td>
                                                                <select name="level" id="level">


                                                                    <g:each in="${grupInstanceList}" status="i" var="grupInstances" >

                                                                        <g:if test="${grupInstances.id < grupInstance.id}">
                                                                            <g:if test="${grupInstances.id == grupInstance2.id}">
                                                                                <option value="${grupInstance2.id}" selected>${fieldValue(bean: grupInstance2, field: "ngrup")} </option>
                                                                            </g:if>
                                                                            <g:else>
                                                                                <option datatype="number" value="${grupInstances.id}">${fieldValue(bean: grupInstances, field: "ngrup")}</option>

                                                                            </g:else>

                                                                        </g:if>


                                                                    </g:each>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </g:if>
                                                </g:else>
                                            </g:if>
                                            <tr>
                                                <td style="font-weight: bold;width: 100px;height: 30px;">
                                                   Max
                                                </td>
                                                <td style="font-weight: bold;">
                                                    :
                                                </td>
                                                <td>
                                                    <input type="text" name="batas" id="batas" value="${grupInstance.batas}">
                                                </td>
                                            </tr>






                                                <tr>
                                                    <g:hiddenField name="id" value="${grupInstance.id}" />
                                                    <g:hiddenField name="version" value="${grupInstance.version}" />

                                                    <td colspan="3" style="text-align: right" >
                                                        <div style="margin-top: 20px;">
                                                            <g:submitButton name="save" value="Update" class="btn btn-info"></g:submitButton>

                                                        </div>

                                                    </td>
                                                </tr>
</g:form>
                                        </table>

                                    </center>
                                </div>

                            </center>
                        </div>

                    </div>



</body>
</html>