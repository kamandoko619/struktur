<%--
  Created by IntelliJ IDEA.
  User: handoko
  Date: 12/08/2016
  Time: 10.52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="layout" content="themablue">

</head>
<body>

                    <div class="kotak" style="padding-bottom: 30px;padding-top: 5px">
                        <div id="create-author" class="content scaffold-create" role="main">
                            <center>
                                <div id="show-author" class="content scaffold-show" role="main">
                                    <center>

                                        <table style="width: 300px;margin-top: 20px;" >

                                            <g:if test="${pegawaiInstance?.nama}">
                                                <tr>
                                                    <td colspan="3" style="text-align: center;font-weight: bold;font-size: 20px;">
                                                        Data <g:fieldValue bean="${pegawaiInstance}" field="nama"/>
                                                    </td>
                                                </tr>
                                            </g:if>
                                            <g:if test="${pegawaiInstance?.nama}">
                                                <tr>
                                                    <td style="font-weight: bold;width: 100px;height: 30px;">
                                                        <g:message code="pegawai.nama.label" default="Nama" />
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td>
                                                        <g:fieldValue bean="${pegawaiInstance}" field="nama"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold;width: 100px;height: 30px;">
                                                        Jabatan
                                                    </td>
                                                    <td style="font-weight: bold;">
                                                        :
                                                    </td>
                                                    <td>
                                                        <g:fieldValue bean="${pegawaiInstance}" field="jabatan" />
                                                    </td>
                                                </tr>
                                            </g:if>






                                            <g:form>
                                                <tr>
                                                    <g:hiddenField name="id" value="${pegawaiInstance.id}" />
                                                    <td colspan="3" style="text-align: right" >
                                                        <div style="margin-top: 20px;">
                                                            <g:actionSubmit class="delete btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

                                                            <g:link class="edit btn btn-default" action="ubah" id="${pegawaiInstance?.id}"><g:message code="default.button.edit.label" default="Ubah" /></g:link>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </g:form>
                                        </table>

                                    </center>
                                </div>

                            </center>
                        </div>

                    </div>


</body>
</html>