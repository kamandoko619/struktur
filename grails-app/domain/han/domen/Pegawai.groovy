package han.domen

class Pegawai {
    String nama
    Integer jabatan
    String deleteFlag = "N"
    static constraints = {
        jabatan blank: true
    }
}
